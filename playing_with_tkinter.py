# -*- coding: utf-8 -*-
"""
Created on Fri May 25 15:09:01 2018

@author: devpa499
"""
#import Tkinter as tk #Importing the Tkinter module to build the window
#
#root = tk.Tk() #Creating the root widget. This is a window with a title bar and other decoration provided by the window manager. This has to be created before any other widgets and there can only be one root widget.
#
#w = tk.Label(root, text="Hello Tkinter!") #Label widget, the first parameter of the label call is the name of the parent window ("root") making the label widget a child of the root widget.
#w.pack() #Pack tells Tk to fir the size of the window to the given text
#
#root.mainloop() #The window won't appear until we enter the Tkinter event loop and will remain in the event loop until we close the window.

##

#import Tkinter as tk
#
#counter = 0 
#def counter_label(label):
#  def count():
#    global counter
#    counter += 1
#    label.config(text=str(counter))
#    label.after(1000, count)
#  count()
# 
# 
#root = tk.Tk()
#root.title("Counting Seconds")
#label = tk.Label(root, fg="green")
#label.pack()
#counter_label(label)
#button = tk.Button(root, text='Stop', width=25, command=root.destroy)
#button.pack()
#root.mainloop()

##

#import Tkinter as tk
#    
#def write_slogan():
#    print("Tkinter is easy to use!")
#
#root = tk.Tk()
#frame = tk.Frame(root)
#frame.pack()
#
#button = tk.Button(frame, 
#                   text="QUIT", 
#                   fg="red",
#                   command=quit)
#button.pack(side=tk.LEFT)
#slogan = tk.Button(frame,
#                   text="Hello",
#                   command=write_slogan)
#slogan.pack(side=tk.LEFT)
#
#root.mainloop()

##

#from Tkinter import *
#
#master = Tk()
#Label(master, text="First Name").grid(row=0)
#Label(master, text="Last Name").grid(row=1)
#
#e1 = Entry(master)
#e2 = Entry(master)
#
#e1.grid(row=0, column=1)
#e2.grid(row=1, column=1)
#
#mainloop( )

##

#!/usr/bin/python3

from tkinter import *

fields = 'Last Name', 'First Name', 'Job', 'Country'

def fetch(entries):
   for entry in entries:
      field = entry[0]
      text  = entry[1].get()
      print('%s: "%s"' % (field, text)) 

def makeform(root, fields):
   entries = []
   for field in fields:
      row = Frame(root)
      lab = Label(row, width=15, text=field, anchor='w')
      ent = Entry(row)
      row.pack(side=TOP, fill=X, padx=5, pady=5)
      lab.pack(side=LEFT)
      ent.pack(side=RIGHT, expand=YES, fill=X)
      entries.append((field, ent))
   return entries

if __name__ == '__main__':
   root = Tk()
   ents = makeform(root, fields)
   root.bind('<Return>', (lambda event, e=ents: fetch(e)))   
   b1 = Button(root, text='Show',
          command=(lambda e=ents: fetch(e)))
   b1.pack(side=LEFT, padx=5, pady=5)
   b2 = Button(root, text='Quit', command=root.quit)
   b2.pack(side=LEFT, padx=5, pady=5)
   root.mainloop()

##

#from Tkinter import *
#fields = ('Annual Rate', 'Number of Payments', 'Loan Principle', 'Monthly Payment', 'Remaining Loan')
#
#def monthly_payment(entries):
#   # period rate:
#   r = (float(entries['Annual Rate'].get()) / 100) / 12
#   print("r", r)
#   # principal loan:
#   loan = float(entries['Loan Principle'].get())
#   n =  float(entries['Number of Payments'].get())
#   remaining_loan = float(entries['Remaining Loan'].get())
#   q = (1 + r)** n
#   monthly = r * ( (q * loan - remaining_loan) / ( q - 1 ))
#   monthly = ("%8.2f" % monthly).strip()
#   entries['Monthly Payment'].delete(0,END)
#   entries['Monthly Payment'].insert(0, monthly )
#   print("Monthly Payment: %f" % float(monthly))
#
#def final_balance(entries):
#   # period rate:
#   r = (float(entries['Annual Rate'].get()) / 100) / 12
#   print("r", r)
#   # principal loan:
#   loan = float(entries['Loan Principle'].get())
#   n =  float(entries['Number of Payments'].get()) 
#   q = (1 + r)** n
#   monthly = float(entries['Monthly Payment'].get())
#   q = (1 + r)** n
#   remaining = q * loan  - ( (q - 1) / r) * monthly
#   remaining = ("%8.2f" % remaining).strip()
#   entries['Remaining Loan'].delete(0,END)
#   entries['Remaining Loan'].insert(0, remaining )
#   print("Remaining Loan: %f" % float(remaining))
#
#def makeform(root, fields):
#   entries = {}
#   for field in fields:
#      row = Frame(root)
#      lab = Label(row, width=22, text=field+": ", anchor='w')
#      ent = Entry(row)
#      ent.insert(0,"0")
#      row.pack(side=TOP, fill=X, padx=5, pady=5)
#      lab.pack(side=LEFT)
#      ent.pack(side=RIGHT, expand=YES, fill=X)
#      entries[field] = ent
#   return entries
#
#if __name__ == '__main__':
#   root = Tk()
#   ents = makeform(root, fields)
#   root.bind('<Return>', (lambda event, e=ents: fetch(e)))   
#   b1 = Button(root, text='Final Balance',
#          command=(lambda e=ents: final_balance(e)))
#   b1.pack(side=LEFT, padx=5, pady=5)
#   b2 = Button(root, text='Monthly Payment',
#          command=(lambda e=ents: monthly_payment(e)))
#   b2.pack(side=LEFT, padx=5, pady=5)
#   b3 = Button(root, text='Quit', command=root.quit)
#   b3.pack(side=LEFT, padx=5, pady=5)
#   root.mainloop()