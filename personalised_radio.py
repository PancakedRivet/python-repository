# Playing with linking audio and text files
# Last updated by Patrick Devane 24/04/19

import random

class Text(object):

    def __init__(self):
        
        with open("radio_text.txt") as f:
            self.text_list = [line.rstrip() for line in f]

        print(self.text_list)

    def message_formatter(self, song):
        
        print(self.text_list[-1].format(song))


class Audio(object):

    def __init__(self):
        
        with open("radio_audio.txt") as f:
            self.audio_list = [line.rstrip() for line in f]

        print(self.audio_list)

class Radio(object):

    def __init__(self, Text, Audio):
        self.Text = Text
        self.Audio = Audio

    def choose_song(self):
        song = random.choice(Radio_Station.Audio.audio_list)
        return song

    def choose_message(self):
        message = random.choice(Radio_Station.Text.text_list)
        return message

    def play_next(self):
        message = self.choose_message()
        song = self.choose_song()

        self.Text.message_formatter(song)

        radio_message = message + " " + song

        print(radio_message)


if __name__ == '__main__':
    Radio_Text = Text()
    Radio_Audio = Audio()
    Radio_Station = Radio(Radio_Text, Radio_Audio)

    Radio_Station.play_next()

    # print(Radio_Station.Text.text_list)
    # print(Radio_Station.Audio.audio_list)

    # print(random.choice(Radio_Station.Audio.audio_list))