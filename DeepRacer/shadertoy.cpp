
const uint trackPointCount = 6u;

const vec2 trackPoints[trackPointCount] = vec2[](
    vec2(0.0, 0.0) * 0.5f + 0.5f,
    vec2(-0.8, 0.4) * 0.5f + 0.5f,
    vec2(-0.9, 0.3) * 0.5f + 0.5f,
    vec2(-0.3, -0.3) * 0.5f + 0.5f,
    vec2(0.9, -0.3) * 0.5f + 0.5f,
    vec2(0.3, 0.3) * 0.5f + 0.5f
);

const float trackRadius = 0.05f;

struct CatmullRomCurve {
    vec2 p0, p1, p2, p3;
    float alpha;
};


	vec2 Remap( float a, float b, vec2 c, vec2 d, float u ) {
		return mix( c, d, ( u - a ) / ( b - a ) );
	}
    

float GetT( float t, float alpha, const vec2 p0, const vec2 p1 )
{
    vec2 d  = p1 - p0;
    float a = dot(d, d); // Dot product
    float b = pow( a, alpha*.5f );
    return (b + t);
}

vec2 CatmullRomCurve_GetPoint(CatmullRomCurve curve, float t) {
    float t0 = 0.0f;
    float t1 = GetT( t0, curve.alpha, curve.p0, curve.p1 );
    float t2 = GetT( t1, curve.alpha, curve.p1, curve.p2 );
    float t3 = GetT( t2, curve.alpha, curve.p2, curve.p3 );
    t = mix( t1, t2, t );
    vec2 A1 = ( t1-t )/( t1-t0 )*curve.p0 + ( t-t0 )/( t1-t0 )*curve.p1;
    vec2 A2 = ( t2-t )/( t2-t1 )*curve.p1 + ( t-t1 )/( t2-t1 )*curve.p2;
    vec2 A3 = ( t3-t )/( t3-t2 )*curve.p2 + ( t-t2 )/( t3-t2 )*curve.p3;
    vec2 B1 = ( t2-t )/( t2-t0 )*A1 + ( t-t0 )/( t2-t0 )*A2;
    vec2 B2 = ( t3-t )/( t3-t1 )*A2 + ( t-t1 )/( t3-t1 )*A3;
    vec2 C  = ( t2-t )/( t2-t1 )*B1 + ( t-t1 )/( t2-t1 )*B2;
    return C;
}

vec2 CatmullRomCurve_GetTangent(CatmullRomCurve curve, float t) {
    const float eps = 0.001f;
    return normalize(CatmullRomCurve_GetPoint(curve, t + eps) - CatmullRomCurve_GetPoint(curve, t - eps));
}

vec2 CatmullRomCurve_GetNormal(CatmullRomCurve curve, float t) {
    vec2 tangent = CatmullRomCurve_GetTangent(curve, t);
    return vec2(-tangent.y, tangent.x);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
   
    CatmullRomCurve curves[trackPointCount];
    for (uint i = 0u; i < trackPointCount; i += 1u) {
        curves[i].p0 = trackPoints[(i + 0u) % trackPointCount];
        curves[i].p1 = trackPoints[(i + 1u) % trackPointCount];
        curves[i].p2 = trackPoints[(i + 2u) % trackPointCount];
        curves[i].p3 = trackPoints[(i + 3u) % trackPointCount];
        curves[i].alpha = 0.5f;
    }
    

    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    float time = fract(0.25f * iTime); // cos(iTime+uv.xyx+vec3(0,2,4)).x * 0.5f + 0.5f;

    // Time varying pixel color
    vec3 col = vec3(0.0f); // 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));

    // Output to screen
    fragColor = vec4(col,1.0);
    
    for (uint i = 0u; i < trackPointCount; i += 1u) {
        if (length(uv - trackPoints[i]) < 0.005f) {
           fragColor = vec4(1, 0, 0, 1);
        }
    }
    
    float segmentLengths[trackPointCount];
    float totalLength = 0.0;
    
    for (uint segment = 0u; segment < trackPointCount; segment += 1u) {
        const uint steps = 40u;
        
        vec2 previousPoint = CatmullRomCurve_GetPoint(curves[segment], 0.0f);
        segmentLengths[segment] = 0.0f;
        
        for (uint i = 0u; i < steps; i += 1u) {
           float segmentT = (float(i) + 0.5f) / float(steps);

           vec2 curvePoint = CatmullRomCurve_GetPoint(curves[segment], segmentT);
           float lengthTravelled = length(curvePoint - previousPoint);
           segmentLengths[segment] += lengthTravelled;
           previousPoint = curvePoint;
       
           vec2 tangent = CatmullRomCurve_GetTangent(curves[segment], segmentT);
           vec2 normal = CatmullRomCurve_GetNormal(curves[segment], segmentT);

           vec2 trackMin = curvePoint - trackRadius * normal;
           vec2 trackMax = curvePoint + trackRadius * normal;

           // ax + by + c = 0;
           // gradient = (a, b)
           // ax + by
           vec3 lineEquation = vec3(tangent, -dot(tangent, curvePoint));
           float distanceToLine = dot(lineEquation, vec3(uv, 1)); // / length(tangent);

           vec2 vectorToPoint = dot(lineEquation, vec3(uv, 1)) * tangent;
           vec2 uvOnLine = uv - vectorToPoint;

           vec2 tOnLineVec = (uvOnLine - trackMin) / (trackMax - trackMin);
           float tOnLine = min(tOnLineVec.x, tOnLineVec.y);

           if (length(uv - curvePoint) <= trackRadius) {
               // fragColor.rg = vec2(1.0f); // = vec4(1, 1, 0, 1);
           }

           if (length(uv - uvOnLine) <= 0.02f && tOnLine == clamp(tOnLine, 0.f, 1.f)) {
               fragColor.b = 0.1f + 0.9f * float(segment + 1u) / float(trackPointCount); // = vec4(1, 0, 1, 1);
           }
        }
        
        vec2 curvePoint = CatmullRomCurve_GetPoint(curves[segment], 1.0f);
        segmentLengths[segment] += length(curvePoint - previousPoint);
        
        totalLength += segmentLengths[segment];
    }
    
    float segmentFractions[trackPointCount + 1u];
    segmentFractions[0] = 0.f;
    for (uint segment = 0u; segment < trackPointCount; segment += 1u) {
        segmentFractions[segment + 1u] = segmentFractions[segment] + segmentLengths[segment] / totalLength;
    }
    segmentFractions[trackPointCount] = 1.0f;
 
    
    uint segment = 0u;
    float segmentT = 0.0f;
    for (uint i = 0u; i < trackPointCount; i += 1u) {
        if (time >= segmentFractions[i]) {
            segment = i;
            segmentT = (time - segmentFractions[i]) / (segmentFractions[i + 1u] - segmentFractions[i]);
        }
    }
    
    //for (uint i = 0u; i < 10u; i += 1u) {
    // uint segment = uint(time * float(trackPointCount));
    // float segmentT = fract(time * float(trackPointCount));
    
        vec2 curvePoint = CatmullRomCurve_GetPoint(curves[segment], segmentT); // (float(i) + 0.5f)/10.f);
        curvePoint -= CatmullRomCurve_GetNormal(curves[segment], segmentT) * trackRadius;
        if (length(uv - curvePoint) < 0.01) {
           fragColor = vec4(0, 1, 1, 1);
        }
    // }
    
}