import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime as datetime
import math as math
import csv

from scipy.special import erf

class polyware_load(object):

    def __init__(self, polyware_file_path):

        f = open(polyware_file_path, "r")
        contents =f.read()
        f.close()

        values = []
        number_start = False
        number = ''

        for i in contents:

            if number_start == False and i != ' ':
                number_start = True
                number = number + i
                continue

            if number_start == True and i != ' ':
                number = number + i

            if number_start == True and i == ' ':
                number_start = False
                completed_number = number
                number = ''
                try:
                    val = float(completed_number)
                    values.append(val)
                except:
                    continue

        self.number_of_runs = int(len(values)/8)

        self.value_array = np.reshape(np.asarray(values), [self.number_of_runs, 8])


class calculate_plots(object):

    def __init__(self, country_name,  polyware_data, country_data, lamda_array):
        
        self.country_name = country_name
        self.polyware_data = polyware_data

        self.data_to_plot = []
        fits = []

        #Adding data for country into one list
        for i in np.arange(polyware_data.number_of_runs):
            self.data_to_plot.append( covid(country_name=self.country_name,
                                            height=self.polyware_data.value_array[i,0],
                                            mean=self.polyware_data.value_array[i,1],
                                            std_dev=self.polyware_data.value_array[i,2],
                                            #lamda=self.polyware_data.value_array[i,3],
                                            lamda=lamda_array[i],
                                            data_csv=country_data) )

            fits.append(self.data_to_plot[i].residual_fit)

        self.best_fit_run = fits.index(min(fits))
        self.worst_fit_run = fits.index(max(fits))

        self.best_lamda = self.data_to_plot[self.best_fit_run].lamda


class covid(object):

    def __init__(self, country_name,  height, mean, std_dev, lamda, data_csv, graph_data=False):
    
        self.country_name = country_name
        self.height = height
        self.mean = mean
        self.std_dev = std_dev
        self.lamda = lamda
        self.data_csv = data_csv

        with open(self.data_csv) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            read_day = []
            read_new_cases = []
        
            for row in csv_reader:
                read_day.append(int(row[0]))
                read_new_cases.append(int(row[1]))

        self.day = np.asarray(read_day)
        self.new_cases = np.asarray(read_new_cases)

        #print(self.new_cases)
        self.calc()

        if graph_data:
            self.graph()


    def calc(self):

        self.invert_lamda = 1 / self.lamda
        self.first_half_equation = self.height * (np.exp((-0.5) * ((self.day-self.mean)/self.std_dev) * ((self.day-self.mean)/self.std_dev)))*(self.std_dev/self.invert_lamda)*(np.sqrt(np.pi/2))
        self.erfcx_value = (1/np.sqrt(2)) * ((self.std_dev/self.invert_lamda)-((self.day-self.mean)/self.std_dev))
        self.erfcx_2 = (np.exp(self.erfcx_value*self.erfcx_value))*(1-erf(self.erfcx_value))
        self.y_value = (self.first_half_equation * self.erfcx_2)

        self.residuals = self.y_value - self.new_cases

        self.residual_fit = sum(np.sqrt(self.residuals ** 2))

        """
        A = x (days)
        B = New Cases
        C = Height
        D = Mean
        E = SD
        F = Lamda
        G = inverted_lamda
        H = first_half_equation
        I = erfcx_value
        J = erfcx_2
        """


    def graph(self):

        plt.plot(self.day, self.new_cases, 'k*-', label='New Cases')
        plt.plot(self.day, self.y_value, 'r*-', label='Model')
        plt.legend()

        plt.xlabel('Days Since Janurary 22 2020')
        plt.ylabel('Daily New Cases')
        plt.title('%s COVID-19 Cases' %self.country_name)
        plt.show()


if __name__ == '__main__':

    # First Country:
    #Loading data from .PW file:
    polyware = polyware_load(polyware_file_path='C:\\Peter\Microsoft Visual Studio\\PolyMigration\\New Zealand\\NewZealand.PW')

    #Loading New Case data from .csv file:
    country_data = 'C:\\Peter\Microsoft Visual Studio\\PolyMigration\\New Zealand\\New_Zealand.csv'

    #Generating data for plotting:
    data_for_plotting = calculate_plots(country_name='New Zealand', polyware_data=polyware, country_data=country_data, lamda_array=polyware.value_array[:,3])

    # Second Country:
    #Loading data from .PW file:
    polyware_CA = polyware_load(polyware_file_path='C:\\Peter\Microsoft Visual Studio\\PolyMigration\\Canada\\CANADA.PW')

    #Loading New Case data from .csv file:
    country_data_CA = 'C:\\Peter\Microsoft Visual Studio\\PolyMigration\\Canada\\Canada.csv'

    #Generating data for plotting:
    data_for_plotting_CA = calculate_plots(country_name='Canada', polyware_data=polyware_CA, country_data=country_data_CA, lamda_array=polyware_CA.value_array[:,3])


    #Generating new data for plotting (Canada with NZ lamda):
    best_lamda_array_NZ = np.repeat(data_for_plotting.best_lamda, polyware_CA.number_of_runs)
    data_for_plotting_CA_NZ = calculate_plots(country_name='Canada', polyware_data=polyware_CA, country_data=country_data_CA, lamda_array=best_lamda_array_NZ)

    print(data_for_plotting.best_lamda)

    #Plotting data:
    
    #New Zealand
    plt.plot(data_for_plotting.data_to_plot[0].day, data_for_plotting.data_to_plot[0].new_cases, 'k*-', label='New Cases')
    plt.plot(data_for_plotting.data_to_plot[data_for_plotting.worst_fit_run].day, data_for_plotting.data_to_plot[data_for_plotting.worst_fit_run].y_value, 'b*-', label='Worst Model')
    plt.plot(data_for_plotting.data_to_plot[data_for_plotting.best_fit_run].day, data_for_plotting.data_to_plot[data_for_plotting.best_fit_run].y_value, 'r*-', label='Best Model')
    
    plt.legend()

    plt.xlabel('Days Since Janurary 22 2020')
    plt.ylabel('Daily New Cases')
    plt.title('%s COVID-19 Cases' %data_for_plotting.country_name)
    plt.show()

    #Canada
    plt.plot(data_for_plotting_CA.data_to_plot[0].day, data_for_plotting_CA.data_to_plot[0].new_cases, 'k*-', label='New Cases')
    plt.plot(data_for_plotting_CA.data_to_plot[data_for_plotting_CA.best_fit_run].day, data_for_plotting_CA.data_to_plot[data_for_plotting_CA.best_fit_run].y_value, 'r*-', label='Worst Model')
    plt.plot(data_for_plotting_CA.data_to_plot[data_for_plotting_CA.best_fit_run].day, data_for_plotting_CA.data_to_plot[data_for_plotting_CA.best_fit_run].y_value, 'b*-', label='Best Model')
    #plt.plot(data_for_plotting_CA_NZ.data_to_plot[data_for_plotting_CA_NZ.worst_fit_run].day, data_for_plotting_CA_NZ.data_to_plot[data_for_plotting_CA_NZ.worst_fit_run].y_value, 'b*-', label='Worst Model')
    plt.plot(data_for_plotting_CA_NZ.data_to_plot[data_for_plotting_CA_NZ.best_fit_run].day, data_for_plotting_CA_NZ.data_to_plot[data_for_plotting_CA_NZ.best_fit_run].y_value, 'g*-', label='Best Model (NZ Lamda)')
    
    plt.legend()

    plt.xlabel('Days Since Janurary 22 2020')
    plt.ylabel('Daily New Cases')
    plt.title('%s COVID-19 Cases' %data_for_plotting_CA_NZ.country_name)
    plt.show()

    """
    polyware_runs = []
    fits = []

    #Adding data for country into one list
    for i in np.arange(polyware.number_of_runs):
        polyware_runs.append( covid(country_name=country,
                                    height=polyware.value_array[i,0],
                                    mean=polyware.value_array[i,1],
                                    std_dev=polyware.value_array[i,2],
                                    lamda=polyware.value_array[i,3],
                                    data_csv=country_data) )

        fits.append(polyware_runs[i].residual_fit)
        #plt.plot(polyware_runs[i].day, polyware_runs[i].y_value, 'r*-', label='Model %s' %(i+1))

    #Loading second data set:

    polyware_CA = polyware_load(polyware_file_path='C:\\Peter\Microsoft Visual Studio\\PolyMigration\\Canada\\CANADA.PW')
    country_data_CA = 'C:\\Peter\Microsoft Visual Studio\\PolyMigration\\Canada\\Canada.csv'

    polyware_runs_CA = []
    fits_CA = []

    #Adding data for country into one list
    for i in np.arange(polyware_CA.number_of_runs):
        polyware_runs_CA.append( covid(country_name='Canada',
                                    height=polyware_CA.value_array[i,0],
                                    mean=polyware_CA.value_array[i,1],
                                    std_dev=polyware_CA.value_array[i,2],
                                    lamda=polyware_CA.value_array[i,3],
                                    data_csv=country_data_CA) )

        fits_CA.append(polyware_runs[i].residual_fit)

    best_fit_run_CA = fits.index(min(fits_CA))
    best_lamda_CA = polyware_runs[best_fit_run_CA].lamda
    best_lamda_array_CA = np.repeat(best_lamda_CA, polyware.number_of_runs)

    #Resuming Graphing:

    best_fit_run = fits.index(min(fits))
    worst_fit_run = fits.index(max(fits))

    best_lamda = polyware_runs[best_fit_run].lamda

    best_lamda_array = np.repeat(best_lamda, polyware.number_of_runs)

    # print(best_lamda_array)

    plt.plot(polyware_runs[0].day, polyware_runs[0].new_cases, 'k*-', label='New Cases')
    plt.plot(polyware_runs[worst_fit_run].day, polyware_runs[worst_fit_run].y_value, 'b*-', label='Worst Model')
    plt.plot(polyware_runs[best_fit_run].day, polyware_runs[best_fit_run].y_value, 'r*-', label='Best Model')
    
    plt.legend()

    plt.xlabel('Days Since Janurary 22 2020')
    plt.ylabel('Daily New Cases')
    plt.title('%s COVID-19 Cases' %country)
    plt.show()
    """

    """
    covid(country_name='New Zealand',
                    height=97.47763,
                    mean=66.09375,
                    std_dev=5.96722,
                    lamda=0.21875,
                    data_csv='C:\\Peter\Microsoft Visual Studio\\PolyMigration\\New Zealand\\New_Zealand.csv',
                    graph_data=True)
    """