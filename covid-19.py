import pyodbc
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime as datetime
import math as math

country = 'New Zealand'
#country = 'Australia'

class covid_summary(object):

    def __init__(self, country):
    
        self.country = country

        server = 'NZL146' 
        database = 'COVID-19' 
        username = 'sa' 
        password = 'Network1' 
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        #cursor = cnxn.cursor()

        # Connecting and reading the data
        #query = 'SELECT * FROM tblWorldData WHERE Confirmed > 0 AND Country = \'%s\'' %self.country
        query = 'SELECT * FROM tblWorldData WHERE Country = \'%s\' ORDER BY 1 ASC' %self.country
        self.data = pd.read_sql_query(query, cnxn)

        self.first_case_date = self.data['Date'].iloc[0]

        #print(val[0])

        #print(self.start_date)

        #Find a starting value for mu
        self.mu = self.data['New'].idxmax(axis=0, skipna=True)

        self.data_range = int(2 * self.mu + 5)

        #NZ Values (Best)
        self.sigma = 6.2
        #self.mu = 68
        self.scale = 1360

        #AU Values
        #self.sigma = 6.8
        #self.mu = 62
        #self.scale = 6000

        cnxn.close()

        self.calc()

        #self.best_fit = self.residual_fit
        
        #Not respecting best value, taking one value past best value and reducing accuracy
        self.disturb(1)
        self.disturb(2)
        self.disturb(3)

        self.minimise_gradient()

        print('Best Fit (residual sum) = %f' %(self.residual_fit))
        print('Mu = %f, Sigma = %f, Scale = %f' %(self.mu, self.sigma, self.scale))

        self.project_cases()
        self.graph()


    def normal_distribution(self, x_data):
        return self.scale * 1/(self.sigma * np.sqrt(2 * np.pi)) * np.exp( - (x_data - self.mu)**2 / (2 * self.sigma**2) )

    def normal_distribution_gradient(self, x_data):
        return self.scale * - (x_data - self.mu)/(self.sigma**3 * np.sqrt(2 * np.pi)) * np.exp( - (x_data - self.mu)**2 / (2 * self.sigma**2) )

    def normal_distribution_gradient_2(self, x_data):
        return self.scale / (self.sigma**3 * np.sqrt(2 * np.pi)) * ( ((x_data - self.mu)**2 /self.sigma**3) - 1 ) * np.exp( - (x_data - self.mu)**2 / (2 * self.sigma**2) )


    def calc(self):

        x_data = self.data['DaysSinceStart']
        #x_data = np.linspace(0, self.data_range, num=self.data_range+1)

        self.norm_dist = self.normal_distribution(x_data)
        #self.norm_dist_grad = self.normal_distribution_gradient(x_data)
        #self.norm_dist_grad_2 = self.normal_distribution_gradient_2(x_data)

        #norm_dist_data = self.norm_dist[:self.data.shape[0]]

        self.residuals = self.data['New'] - self.norm_dist
        #self.residuals = self.data['New'] - norm_dist_data

        self.residual_fit = sum(np.sqrt(self.residuals ** 2))


    def disturb(self, var):

        best_fit = self.residual_fit

        counter = 0
        direction = 0
        sign = 1

        while counter < 100:

            previous_fit = self.residual_fit

            if var == 1:
                self.mu = self.mu + (sign * 1)
            if var == 2:
                self.sigma = self.sigma + (sign * 0.1)
            if var == 3:
                self.scale = self.scale + (sign * 10)

            self.calc()

            if self.residual_fit > previous_fit:
                sign = sign * -1
                direction += 1

            if self.residual_fit < best_fit:
                best_fit = self.residual_fit

            if direction > 1 and self.residual_fit == best_fit:
                break

            counter += 1


    def minimise_gradient(self):

        x_data = np.linspace(0, 100, num=101)

        norm_dist = self.normal_distribution(x_data)
        #norm_dist_grad = self.normal_distribution_gradient(x_data)

        end_day = 0

        for ind, val in enumerate(norm_dist):
            if val < 0.5 and ind > self.mu:
                #print(val)
                #print(ind)
                end_day = ind
                break

        days = abs(self.data['DaysSinceStart'].iloc[0]) + end_day
        end_date = self.data['Date'].iloc[0] + datetime.timedelta(days=int(days))

        print('MIN Earliest date of no new cases in %s = %s' %(self.country, end_date))

        #print(norm_dist)


    def project_cases(self):

        self.end_day = int(self.mu + 3 * self.sigma)

        last_data_day = int(self.data['DaysSinceStart'].iloc[-1])

        days = abs(self.data['DaysSinceStart'].iloc[0]) + self.end_day
        end_date = self.data['Date'].iloc[0] + datetime.timedelta(days=int(days))

        print('Earliest date of no new cases (3 sigma CI) in %s = %s' %(self.country, end_date))
        print('Lowest infectivity duration in %s = %i days' %(self.country, self.end_day) )

        #self.case_distribution = np.linspace(last_data_day, self.end_day, num=self.end_day - last_data_day + 1)
        self.case_distribution = np.linspace(last_data_day, last_data_day + 7, num=8)

        self.case_projection = self.normal_distribution(self.case_distribution)

        self.case_projection_grad = self.normal_distribution_gradient(self.case_distribution)
        #self.case_projection_grad_2 = self.normal_distribution_gradient_2(self.case_distribution)

        self.projected_cases = np.zeros((self.case_distribution.shape[0],))

        for i in range(0,self.case_projection.shape[0]):
            self.projected_cases[i] = self.projected_cases[i-1] + self.case_projection[i]
        
        self.projected_cases = self.projected_cases + self.data['Confirmed'].iloc[-1]

        print('Total confirmed cases in %s = %i' %(self.country, self.projected_cases[-1]) )


    def graph(self):

        x_data = self.data['DaysSinceStart']

        fig, axs = plt.subplots(2)
        fig.suptitle('%s COVID-19 Summary' %country)
        axs[0].plot(x_data, self.data['Confirmed'], 'b*-', label='Confirmed Cases')
        axs[0].plot(self.case_distribution, self.projected_cases, 'r*-', label='Projected Confirmed Cases')
        axs[0].legend()
        axs[1].plot(x_data, self.norm_dist, 'g*-', label='Norm Dist Fit')
        #axs[1].plot(x_data, self.residual_fit, 'r*-', label='Residuals')
        axs[1].plot(self.case_distribution, self.case_projection, 'r*-', label='Projected New Cases')
        axs[1].plot(x_data, self.data['New'], 'k*-', label='New Cases')
        axs[1].legend()

        #axs[2].plot(x_data, self.norm_dist, 'b*-', label='Projected New Cases')
        #axs[2].plot(self.case_distribution, self.case_projection, 'y*-', label='Projected New Cases')

        #axs[2].plot(x_data, self.norm_dist_grad, 'r*-', label='Projected New Cases')
        #axs[2].plot(self.case_distribution, self.case_projection_grad, 'g*-', label='Projected New Cases')

        #axs[3].plot(x_data, self.norm_dist_grad_2, 'k*-', label='Projected New Cases')
        #axs[3].plot(self.case_distribution, self.case_projection_grad_2, 'c*-', label='Projected New Cases')

        plt.xlabel('Days Since Janurary 22 2020')
        plt.ylabel('Cases')
        plt.show()


if __name__ == '__main__':
    summary = covid_summary(country)
