import numpy as np

class NeuralNetwork(object):

    def __init__(self, inputs, outputs, training_iterations, network_layers, hidden_neurodes_per_layer=0):
        
        input_size = inputs[0].size
        output_size = outputs[0].size

        np.random.seed(1)

        self.layer_weights = []

        if network_layers <= 2: # building a perceptron
            weight = 2*np.random.random((input_size,output_size)) - 1
            self.layer_weights.append(weight)
        else: # building a neural network
            weight_input = 2*np.random.random((input_size,hidden_neurodes_per_layer)) - 1
            weight_output = 2*np.random.random((hidden_neurodes_per_layer,output_size)) - 1
            self.layer_weights.append(weight_input)

            for dummy in range(1, network_layers):
                weight_hidden = 2*np.random.random((hidden_neurodes_per_layer,hidden_neurodes_per_layer)) - 1
                self.layer_weights.append(weight_hidden)

            self.layer_weights.append(weight_output)

        self.network_training(inputs, outputs, training_iterations)

    def nonlin(self,x,deriv=False):
        if deriv:
            return x * (1-x)
        return 1 / (1+np.exp(-x))

    def feed_forward(self, x):
        
        self.feed_forward_results = []
        self.feed_forward_results.append(x)

        for j in range(1, len(self.layer_weights)+1):
            next_layer_results = self.nonlin(np.dot(self.feed_forward_results[j-1],self.layer_weights[j-1]))
            self.feed_forward_results.append(next_layer_results)

    def back_propogation(self, y=0):
        
        self.error = []
        self.delta = []

        for j in range(len(self.layer_weights)):
            if j == 0:
                error = y - self.feed_forward_results[-1]
            else:
                error = self.delta[j-1].dot(self.layer_weights[-j].T)

            self.error.append(error)

            delta = error * self.nonlin(self.feed_forward_results[-1-j],deriv=True)
            self.delta.append(delta)

        self.error.reverse()
        self.delta.reverse()

    def weight_adjustment(self):

        for j in range(len(self.layer_weights)):
            self.layer_weights[j] += self.feed_forward_results[j].T.dot(self.delta[j])

    def network_training(self, x, y, iterations):

        for dummy in range(iterations):

            self.feed_forward(x)
            self.back_propogation(y)
            self.weight_adjustment()

    def network_sample(self, input_size):
        new_input = np.zeros((1,input_size))
        print("Enter %s new inputs:" %input_size)
        for j in range(input_size):
            input_value = input("New input %s: " %str(j+1))
            new_input[0,j] = input_value
        print("Input to sample = ", new_input)
        new_input = new_input.astype(float)
        self.feed_forward(new_input)
        print(self.feed_forward_results[-1])

    
if __name__ == "__main__":

    training_inputs = np.array([ [0,0,1],
                                 [0,1,0],
                                 [1,0,1],
                                 [1,1,1] ])

    training_outputs = np.array([ [0],
                                  [0],
                                  [1],
                                  [1] ])

    Neural_Network = NeuralNetwork(training_inputs, training_outputs, 60000, network_layers=3, hidden_neurodes_per_layer=4)
    Neural_Network.network_sample(input_size=training_inputs[0].size)