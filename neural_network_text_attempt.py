# Attempting to build my own neural net
# Last updated by Patrick Devane on 24/04/19

# Use will be in text analysis
# Method is generating probabilities of characters in succession
# Training will be done on pokemon name list so it will generate new pokemon names
# This should be done as a Recurraent Neural Network (RNN)

# Useful Articles
# https://lifehacker.com/we-trained-an-ai-to-generate-lifehacker-headlines-1826616918
# https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
# http://karpathy.github.io/2015/05/21/rnn-effectiveness/
# https://github.com/karpathy/char-rnn
# http://www.wildml.com/2015/09/implementing-a-neural-network-from-scratch/
# https://towardsdatascience.com/regularization-in-machine-learning-76441ddcf99a
# https://www.reddit.com/r/MachineLearning/
# https://medium.com/technology-invention-and-more/how-to-build-a-simple-neural-network-in-9-lines-of-python-code-cc8f23647ca1
# http://iamtrask.github.io/2015/07/12/basic-python-network/

# https://gist.github.com/aparrish/2f562e3737544cf29aaf1af30362f469

# https://www.reddit.com/r/MachineLearning/comments/bhdmet/p_this_video_shows_you_how_to_build_a_nn_for/

# chars = list(set('Python'))

# ix_to_char = { j:ch for j,ch in enumerate(chars) }
# char_to_ix = { i for i in enumerate(chars) }

# print(chars)
# print(ix_to_char)
# print(char_to_ix)

# print(list('Pythonpatrick'))
# print(set('Pythonpatrick'))

import numpy as np

# data I/O
data = open('pokemon.txt', 'r').read() # should be simple plain text file
chars = list(set(data)) # list returns sequence list of elements, set makes it distinct chars only
data_size, vocab_size = len(data), len(chars)
print('data has %d characters, %d unique.' % (data_size, vocab_size))
char_to_ix = { ch:i for i,ch in enumerate(chars) } # dictionary of characters in the list
ix_to_char = { i:ch for i,ch in enumerate(chars) }

print(char_to_ix)
print(ix_to_char)