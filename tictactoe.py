# TicTacToe Console Game
# By Patrick Devane

class Game_Board(object):

    def __init__(self, size, Players):
        
        self.size = size
        self.game_board_array = []
        self.max_value = size ** 2

        for dummy in range(self.max_value):
            self.game_board_array.append('.')

        self.current_turn = 1
        self.game_over = False

        self.Players = Players

        print('\nWelcome to Tic-Tac-Toe\n')

        print('Gameboard move positions are shown below:')
        print('1 2 .')
        print('. . .\n')

        self.new_game()

    def new_game(self):

        self.game_board_array = []

        for dummy in range(self.max_value):
            self.game_board_array.append('.')

        print('Game board is clear:')
        self.draw_board()

        self.play()

    def play(self):

        while self.current_turn < (self.size ** 2) + 1:
            for i in range(len(self.Players)):
                if not self.game_over:
                    self.enter_position(self.Players[i])
                else:
                    break
            if self.game_over:
                break
            else:
                continue

        print('\nGame Over!')

    def enter_position(self, Player):

        print('Turn %i: (%i turns until draw)' %(self.current_turn, self.max_value - self.current_turn))
        print('Player %i\'s turn! (%s)' %(Player.number, Player.icon))
        
        while 1 == 1:
            x1 = int(input('Enter a location for your move: ')) - 1

            if x1 > self.max_value or x1 < 0:
                print('Invalid position chosen!')

            if str(self.game_board_array[x1]) == '.':
                self.game_board_array[x1] = Player.icon
                break
            else:
                print('Position already taken!')
                continue
                
        self.current_turn = self.current_turn + 1
        self.draw_board()
        self.win_check(Player)
        print('')

    def draw_board(self):

        i = 0
        print('')
        for dummy in range(self.size):
            print(*self.game_board_array[i:i+self.size], sep=' ')
            i += self.size
        print('')

    def win_check(self, Player):

        win_condition = []
        check_dia1 = []
        check_dia2 = []

        for i in range(self.size):
            win_condition.append(Player.icon)

        for i in range(self.size):
            check_col = []
            check_row = []

            for j in range(self.size):
                check_col.append(self.game_board_array[i + j * self.size])
                check_row.append(self.game_board_array[i * self.size + j])

            if win_condition == check_col or win_condition == check_row:
                print('Player %s is the winner!' %Player.number)
                self.game_over = True
            
            check_dia1.append(self.game_board_array[i * (self.size + 1)])
            check_dia2.append(self.game_board_array[i * (self.size - 1) + (self.size - 1)])

        if win_condition == check_dia1 or win_condition == check_dia2:
            print('Player %s is the winner!' %Player.number)
            self.game_over = True
        
        if self.current_turn > (self.size ** 2):
            print('The game is a draw!')
            self.game_over = True


class Player(object):

    def __init__(self, number, icon):

        self.number = int(number)
        self.icon = str(icon)


if __name__ == "__main__":

    Players = []

    Players.append(Player(number=1, icon='O'))
    Players.append(Player(number=2, icon='X'))
    #Players.append(Player(number=3, icon='M'))

    Game1 = Game_Board(size=3, Players=Players) 

    while 1 == 1:

        retry = input('Press p to play again: ')
        
        if retry == 'p' or retry == 'P':
            Game1 = Game_Board(size=3, Players=Players) 
        else:
            break
