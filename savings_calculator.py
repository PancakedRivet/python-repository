# Interest Calculator
# Made by Patrick Devane
# Last Updated: 15/04/19

# List of things to add:
#
# Comparing values to true values to esnure it's accurate
# Build an optimiser for how to split funds between home and savings, goal being most money saved, done using a fixed total e.g. $2500 to distribute between schemes
# Need solvers to account for determining when a certain level is reached e.g. when interest = x
# Kiwisaver Savings model:
# - including the government member tax credit of $0.50 matched for every $1 spent up to a total of $1042.85 (meaning govt contribution is $521.43)
# - Look into percentages: Employees contribute [3, 4, 6, 8, 10]% and employers contribute minimum 3%. This is of gross pay but tax is still paid on full mount earned. 
#     For example, if you earned $100 and had 8% ($8) KiwiSaver contributions deducted, you would still pay tax on the full $100

# Useful articles:
# https://stackoverflow.com/questions/6916978/how-do-i-tell-matplotlib-to-create-a-second-new-plot-then-later-plot-on-the-o
# https://www.kiwisaver.govt.nz/providers/ks-providers.html

import numpy as np
import matplotlib.pyplot as plt

class Savings_Account_Calculator(object):

    def __init__(self, scheme_name, principle, interest_rate_pa, pie_tax_rate, interest_increment_MO, deposit_amount, deposit_interval_MO):
        
        self.scheme_name = scheme_name
        self.principle = principle
        self.interest_rate_pa = interest_rate_pa / 100
        self.pie_tax_rate = pie_tax_rate / 100
        self.interest_increment_MO = interest_increment_MO
        self.deposit_amount = deposit_amount
        self.deposit_interval_MO = deposit_interval_MO

        if self.deposit_interval_MO == 0:
            self.deposit_interval_MO = 1
            self.deposit_amount = 0

    def calculate_savings(self, interest_increments_to_calculate, print_results=False, graph_results=False):

        self.calculate_return_values = np.zeros((interest_increments_to_calculate+1, 7))

        self.calculate_return_values[0,0] = self.principle

        starting_principle = self.principle * 100
        current_calculation_row = np.zeros((7))

        deposit_counter = 1

        for i in range(1, interest_increments_to_calculate+1):

            previous_principle = self.calculate_return_values[i-1,0] * 100

            if int(deposit_counter / self.deposit_interval_MO) > 0:
                current_calculation_row[1] = self.deposit_amount * 100 # deposit_amount
                deposit_counter = 0
            else:
                current_calculation_row[1] = 0 # deposit_amount

            current_calculation_row[0] = np.around(previous_principle + current_calculation_row[1]) # principle
            current_calculation_row[2] = np.around((current_calculation_row[0] * self.interest_rate_pa) * (self.interest_increment_MO / 12)) # interest_added
            current_calculation_row[3] = np.around(current_calculation_row[2] * self.pie_tax_rate) # tax_on_interest
            current_calculation_row[4] = current_calculation_row[2] - current_calculation_row[3] # net_interest
            current_calculation_row[0] += current_calculation_row[4]
            current_calculation_row[5] = current_calculation_row[0] - starting_principle # abs_gain
            current_calculation_row[6] = current_calculation_row[6] + current_calculation_row[1] # deposit_total

            self.calculate_return_values[i,:] = current_calculation_row / 100

            deposit_counter += 1

        if print_results:
            self.print_savings()
        if graph_results:
            self.graph_savings()

    def print_savings(self):
        
        print("Scheme Name: %s" %self.scheme_name)

        for i in range(self.calculate_return_values.shape[0]):

            print('Increment #: %d, Return: $%.2f, Abs Gain: $%.2f, Interest: $%.2f, Tax: $%.2f, Net Interest: $%.2f, Deposit Total: $%.2f' %(i, 
                                                                                                                            self.calculate_return_values[i,0],
                                                                                                                            self.calculate_return_values[i,5],
                                                                                                                            self.calculate_return_values[i,2],
                                                                                                                            self.calculate_return_values[i,3],
                                                                                                                            self.calculate_return_values[i,4],
                                                                                                                            self.calculate_return_values[i,6]) )
        print('')

    def graph_savings(self):

        x = range(0, self.calculate_return_values.shape[0])
        plt.plot(x, self.calculate_return_values[:,0], 'kx-', label='Return')
        plt.plot(x, self.calculate_return_values[:,4], 'ro-', label='Net Interest')
        plt.plot(x, self.calculate_return_values[:,6], 'bo-', label='Deposit Total')
        plt.xlabel('Increment #')
        plt.ylabel('$')
        plt.title(self.scheme_name)
        plt.legend()
        plt.show()

class Loan_Calculator(object):

    def __init__(self, scheme_name, principle, interest_rate_pa, payment_frequency_MO, amount_paid_MO, total_amount_paid=0, total_payments=0):
    
        self.scheme_name = scheme_name
        self.principle = principle
        self.interest_rate_pa = interest_rate_pa / 100
        self.payment_frequency_MO = payment_frequency_MO
        self.amount_paid_MO = amount_paid_MO

        self.total_payments = total_payments
        self.total_amount_paid = total_amount_paid
        self.calculate_loan_values = np.array([0, 0, 0, self.principle, self.total_amount_paid])

    def calculate_loan(self, term_MO=0, print_results=False, graph_results=False):

        interest_to_pay = self.principle * self.interest_rate_pa * (self.payment_frequency_MO / 12)

        if self.amount_paid_MO < interest_to_pay:
            print('Loan repayments too low, loan duration is infinite')
            return

        if term_MO == 0:
            self.calculate_loan_for_duration()
        else:
            self.calculate_loan_for_term(term_MO)

        if print_results:
            self.print_loan()
        if graph_results:
            self.graph_loan()

    def calculate_loan_once(self):

        updating_records = np.zeros(5)

        interest = np.around(self.principle * self.interest_rate_pa * (self.payment_frequency_MO / 12), 2)
        net_loan_payment = np.around(self.amount_paid_MO - interest, 2)

        if net_loan_payment > self.principle:
            self.amount_paid_MO = self.principle + interest
            net_loan_payment = np.around(self.amount_paid_MO - interest, 2)

        self.principle = np.around(self.principle - net_loan_payment, 2)
        self.total_amount_paid += self.amount_paid_MO

        updating_records[0] = self.amount_paid_MO
        updating_records[1] = interest
        updating_records[2] = net_loan_payment
        updating_records[3] = self.principle
        updating_records[4] = self.total_amount_paid

        self.calculate_loan_values = np.vstack([self.calculate_loan_values, updating_records])

        self.total_payments += 1

    def calculate_loan_for_duration(self):

        while self.principle > 0:
            self.calculate_loan_once()

        payment_length_years = self.total_payments / (12 / self.payment_frequency_MO)
        payment_length_years_formatted = np.ceil(payment_length_years * 100) / 100

        print("Loan will be paid off in %s years, with total cost $%s." %(payment_length_years_formatted, self.calculate_loan_values[-1,4]))

    def calculate_loan_for_term(self, term_MO):

        total_payments = int(term_MO / self.payment_frequency_MO)

        for dummy in range(total_payments):
            self.calculate_loan_once()

    def print_loan(self):

        print("Scheme Name: %s" %self.scheme_name)

        for i in range(self.calculate_loan_values.shape[0]):
            
            print('Payment #: %d, Amount paid: $%.2f, Interest paid: $%.2f, Principle paid: $%.2f, Loan Remaining: $%.2f, Total amount paid: $%.2f' %(i, 
                                                                                                                                            self.calculate_loan_values[i,0],
                                                                                                                                            self.calculate_loan_values[i,1],
                                                                                                                                            self.calculate_loan_values[i,2],
                                                                                                                                            self.calculate_loan_values[i,3],
                                                                                                                                            self.calculate_loan_values[i,4]) )
        print('')

    def graph_loan(self):

        x = range(0, self.calculate_loan_values.shape[0])
        plt.plot(x, self.calculate_loan_values[:,3], 'kx-', label='Loan Remaining')
        plt.plot(x, self.calculate_loan_values[:,1], 'ro-', label='Interest')
        plt.plot(x, self.calculate_loan_values[:,2], 'b^-', label='Principle')
        plt.plot(x, self.calculate_loan_values[:,4], 'g*-', label='Total Amount')
        plt.xlabel('Payment #')
        plt.ylabel('$')
        plt.title(self.scheme_name)
        plt.legend()
        plt.show()


if __name__ == '__main__':

    NoticeSaver30 = Savings_Account_Calculator(scheme_name='Kiwibank Notice Saver - 32 Day',
                                    principle=10000, 
                                    interest_rate_pa=12, # %
                                    pie_tax_rate=10, # % 
                                    interest_increment_MO=1, # 1=interest calculated monthly
                                    deposit_amount=1000,
                                    deposit_interval_MO=1) # 1=monthly deposit

    NoticeSaver90 = Savings_Account_Calculator(scheme_name='Kiwibank Notice Saver - 90 Day',
                                    principle=100000, 
                                    interest_rate_pa=3.25, # %
                                    pie_tax_rate=10.5, # % 
                                    interest_increment_MO=1, # 1=interest calculated monthly
                                    deposit_amount=2000,
                                    deposit_interval_MO=1) # 1=monthly deposit

    Stocks = Savings_Account_Calculator(scheme_name='Stock Market',
                                    principle=9000, 
                                    interest_rate_pa=10, # %
                                    pie_tax_rate=0, # % 
                                    interest_increment_MO=1, # 1=interest calculated monthly
                                    deposit_amount=0,
                                    deposit_interval_MO=1) # 1=monthly deposit

    HomeLoan = Loan_Calculator(scheme_name="Home Loan",
                                principle=400000,
                                interest_rate_pa=3.96,
                                payment_frequency_MO=1,
                                amount_paid_MO=1900)

    # NoticeSaver30.calculate_savings(interest_increments_to_calculate=6, print_results=True, graph_results=True) 
    # NoticeSaver90.calculate_savings(interest_increments_to_calculate=24, graph_results=True, print_results=True)
    # HomeLoan.calculate_loan(term_MO=24, graph_results=True, print_results=True)
    Stocks.calculate_savings(interest_increments_to_calculate=24, graph_results=True, print_results=True)