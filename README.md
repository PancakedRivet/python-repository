private-projects README
========================

Current Projects
-----------------

- Stock Tracker:

This project will hopefully allow one to track the last user set amount of time, while also searching for news related to a particular stock if there's a sudden rise or fall in stock. This will generate a list of articles or news stories and output it for the user to read through.