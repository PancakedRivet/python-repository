# Services Neural Network Tests / Project (Cleaning Text Date from Support Site)

import re
"""
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Masking, Embedding
from keras.callbacks import EarlyStopping, ModelCheckpoint
"""

"""
def text_filters(old_text):

    new_text = re.sub('&lt;pre.*/pre&gt;', '', old_text)    # Removing code blocks
    new_text = re.sub('&amp;|&nbsp;|nbsp;|(&lt;.+?&gt;)|<.+?>', ' ', new_text)   # Removing tags
    new_text = new_text.replace('&#39;','\'')   # Correcting apostrophe

    return new_text
"""

def text_filters(text):

    replace_list = {r"i'm": 'i am',
                r"'re": ' are',
                r"let’s": 'let us',
                r"'s":  ' is',
                r"'ve": ' have',
                r"can't": 'can not',
                r"cannot": 'can not',
                r"shan’t": 'shall not',
                r"haven't": 'have not',
                r"n't": ' not',
                r"'d": ' would',
                r"'ll": ' will',
                r"'scuse": 'excuse',
                ',': ' ,',
                ':': ' :',
                '.': ' .',
                '!': ' !',
                '?': ' ?',
                '\s+': ' '}

    text = re.sub('&lt;pre.*/pre&gt;', '', text)    # Removing code blocks
    text = re.sub('&amp;|&nbsp;|nbsp;|(&lt;.+?&gt;)|<.+?>', ' ', text)   # Removing tags
    text = text.replace('&#39;','\'')   # Correcting apostrophe
    text = text.lower()

    for s in replace_list:
        text = text.replace(s, replace_list[s])
    text = ' '.join(text.split())

    text = re.sub('varnosafaderani services manage.+$', '', text)   # Removing email chain - Mack specific

    return text

if __name__ == '__main__':
    
    new_item = []
    with open("issue_updates_preclean_mack.txt", "r", encoding="utf-8") as f:
        for item in f:
            new_item.append(text_filters(item))

    with open("issue_updates_clean_mack.txt","w+") as f:
        for item in new_item:
            try:
                f.write(str(item + '\n'))
            except:
                pass

    """
    ## Building and fitting tokeniser:

    t = Tokenizer()
    t.fit_on_texts(new_item)

    sequences = t.texts_to_sequences(new_item)
    
    idx_word = t.index_word

    # print(sequences[0])
    # print(' '.join(idx_word[w] for w in sequences[0]))

    ## Creating Features and Labels:

    features = []
    labels = []

    training_length = 5

    # Iterate through the sequences of tokens
    for seq in sequences:

        # Create multiple training examples from each sequence
        for i in range(training_length, len(seq)):
            
            # Extract the features and label
            extract = seq[i - training_length:i + 1]

            # Set the features and label
            features.append(extract[:-1])
            labels.append(extract[-1])
            
    features = np.array(features)

    ## One-Hot encoding:

    num_words = len(idx_word) + 1

    label_array = np.zeros((len(features), num_words), dtype=np.int8)

    for example_index, word_index in enumerate(labels):
        label_array[example_index, word_index] = 1

    # print(label_array.shape)
    # print(label_array[0])
    # print(idx_word[np.argmax(label_array[0])])

    ## Loading the embeddings from Glove pack:
    # Load in embeddings
    glove_vectors = '/home/ubuntu/.keras/datasets/glove.6B.100d.txt'
    glove = np.loadtxt(glove_vectors, dtype='str', comments=None)

    # Extract the vectors and words
    vectors = glove[:, 1:].astype('float')
    words = glove[:, 0]

    # Create lookup of words to vectors
    word_lookup = {word: vector for word, vector in zip(words, vectors)}

    # New matrix to hold word embeddings
    embedding_matrix = np.zeros((num_words, vectors.shape[1]))

    for i, word in enumerate(idx_word.keys()):
        # Look up the word embedding
        vector = word_lookup.get(word, None)

        # Record in matrix
        if vector is not None:
            embedding_matrix[i + 1, :] = vector

    ## Code for a simple LSTM:
    model = Sequential()

    # Embedding layer
    model.add(
        Embedding(input_dim=num_words,
                input_length = training_length,
                output_dim=100,
                weights=[embedding_matrix],
                trainable=False,
                mask_zero=True))

    # Masking layer for pre-trained embeddings
    model.add(Masking(mask_value=0.0))

    # Recurrent layer
    model.add(LSTM(64, return_sequences=False, 
                dropout=0.1, recurrent_dropout=0.1))

    # Fully connected layer
    model.add(Dense(64, activation='relu'))

    # Dropout for regularization
    model.add(Dropout(0.5))

    # Output layer
    model.add(Dense(num_words, activation='softmax'))

    # Compile the model
    model.compile(
        optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])


    ## Saving model fitting progress
    # Create callbacks
    callbacks = [EarlyStopping(monitor='val_loss', patience=5),
                ModelCheckpoint('services_neural_net_best_model.h5',
                                            save_best_only=True, 
                                            save_weights_only=False)]

    ## Fitting the model:
    history = model.fit(X_train,  y_train, 
                    batch_size=2048, epochs=150,
                    callbacks=callbacks,
                    validation_data=(X_valid, y_valid))

    ## Checking the output
    seed_html, gen_html, a_html = generate_output(model, sequences, idx_word)
    HTML(seed_html)
    HTML(gen_html)
    HTML(a_html)
    """