# Following https://towardsdatascience.com/simple-text-generation-using-lstm-deep-learning-d9ba808905ff

from numpy import array
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Embedding
from tensorflow.keras.models import load_model


class Preprocessing():
    
    def __init__(self,input_file):
        self.input_data_file = input_file
        self.data = None
        self.vocab_size = None
        self.encoded_data = None
        self.max_length = None
        self.sequences = None
        self.x = None
        self.y = None
        self.tokenizer = None
    
    def load_data(self):
        fp = open(self.input_data_file,'r')
        self.data = fp.read().splitlines()        
        fp.close()
        
    def encode_data(self):
        self.tokenizer = Tokenizer()
        self.tokenizer.fit_on_texts(self.data)
        self.encoded_data = self.tokenizer.texts_to_sequences(self.data)
        # print(self.encoded_data)
        self.vocab_size = len(self.tokenizer.word_counts)+1
        
    def generate_sequence(self):
        seq_list = list()
        for item in self.encoded_data:
            l = len(item)
            for id in range(1,l):
                seq_list.append(item[:id+1])
        self.max_length = max([len(seq) for seq in seq_list])
        self.sequences = pad_sequences(seq_list, maxlen=self.max_length, padding='pre')
        # print(self.sequences)
        self.sequences = array(self.sequences)
            
    def get_data(self):
        self.x = self.sequences[:,:-1]
        self.y = self.sequences[:,-1]
        print("y before:",self.y)
        self.y = to_categorical(self.y,num_classes=self.vocab_size)
        print("y After:",self.y)


class Model():

    def __init__(self,params):
        self.model = None
        self.history = None
        self.x = None
        self.y = None
        self.vocab_size = params['vocab_size']
        self.max_len = params['max_len']
        self.activation = params['activation']
        self.optimizer = params['optimizer']
        self.epochs = params['epochs']
        self.metrics = params['metrics']
        
    def create_model(self):
        self.model = Sequential()
        self.model.add(Embedding(self.vocab_size,10,input_length=self.max_len-1))
        self.model.add(LSTM(50))
        self.model.add(Dropout(0.1))
        self.model.add(Dense(self.vocab_size,activation=self.activation))
        self.model.compile(loss='categorical_crossentropy',optimizer=self.optimizer,metrics=self.metrics)
        print(self.model.summary())

    def run(self):
        self.history = self.model.fit(self.x,self.y,epochs=self.epochs)
        self.save()
        
    def save(self):
        self.model.save("lang_model_mack_new.h5")
    
    def load_model(self):
        self.model = load_model("lang_model_mack_new.h5")


class Prediction():

    def __init__(self,tokenizer,max_len):
        self.model = None
        self.tokenizer = tokenizer
        self.idx2word = {v:k for k,v in self.tokenizer.word_index.items()}
        self.max_length = max_len
    
    def load_model(self):
        self.model = load_model("lang_model_mack.h5")
        
    def predict_sequnce(self,text,num_words):
        for id in range(num_words):
            encoded_data = self.tokenizer.texts_to_sequences([text])[0]
            padded_data = pad_sequences([encoded_data],maxlen = self.max_length-1,padding='pre')
            y_pred = self.model.predict(padded_data)
            y_pred = np.argmax(y_pred)
            predict_word = self.idx2word[y_pred]
            text += ' ' + predict_word
        return text


pr = Preprocessing('issue_updates_clean_mack.txt')
pr.load_data()
pr.encode_data()
pr.generate_sequence()
pr.get_data()

# Comment block
params = {"activation":"softmax","epochs":30,"verbose":2,"loss":"categorical_crossentropy",
          "optimizer":"adam","metrics":['accuracy'],"vocab_size":pr.vocab_size,"max_len":pr.max_length}
model_obj = Model(params)
model_obj.x = pr.x
model_obj.y = pr.y
model_obj.create_model()

# Loading the previously trained model
# model_obj.load_model()
# model_obj.epochs = 4    # Overwriting the previously used number of epochs defined in params

model_obj.run()
# model_obj.save()
# Uncomment block
"""
pred = Prediction(pr.tokenizer,pr.max_length)
pred.load_model()

print()
print(pred.predict_sequnce("hi", 20))
print(pred.predict_sequnce('could', 20))
print(pred.predict_sequnce('please', 20))
print(pred.predict_sequnce("screenshot", 20))
print(pred.predict_sequnce("let", 20))
print(pred.predict_sequnce("me", 20))
print(pred.predict_sequnce('log', 20))
print()
print(pred.predict_sequnce('jordan', 20))
print(pred.predict_sequnce('alex', 20))
print()
print(pred.predict_sequnce('kiosk', 20))
print(pred.predict_sequnce('loyalty', 20))
print(pred.predict_sequnce('conect', 20))
print(pred.predict_sequnce('print', 20))
print()
print(pred.predict_sequnce('sorry', 20))
print(pred.predict_sequnce('apologies', 20))
print(pred.predict_sequnce('problem', 20))
print()
print(pred.predict_sequnce('hi mark', 20))
print(pred.predict_sequnce('hi gabi', 20))
print(pred.predict_sequnce('hi patrick', 20))

print()
print(pred.predict_sequnce("excellent news", 50))

# Best Results:
# Accuracy .2737: hi patrick excellent news i haven't heard anything more from you on this so i assume that the issue has been resolved
"""