import pyodbc
import numpy as np
import pandas as pd
import csv

def sqlConnection():

    server = 'NZL146' 
    database = 'COVID-19' 
    username = 'sa' 
    password = 'Network1' 
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return cnxn

def createFile(query, file_name, cnxn):

    data = pd.read_sql_query(query, cnxn)
    file_path = 'C:\\Users\\Patrick.Devane\\Documents\\Git\\python-repository\\covid-datasets\\%s.csv' %file_name

    with open(file_path, 'w+', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(zip(data['DaysSinceStart'],data['New']))
    
    return


if __name__ == '__main__':
    connection = sqlConnection()

    #World Data
    createFile(query='SELECT DaysSinceStart, New FROM uvw_newCasesWorld_ExclChina ORDER BY Date ASC', file_name='WORLD_ExcChina' , cnxn=connection)
    createFile(query='SELECT DaysSinceStart, New FROM uvw_newCasesWorld_InclChina ORDER BY Date ASC', file_name='WORLD_IncChina' , cnxn=connection)

    #Country-Specific Data
    countries = pd.read_sql_query('SELECT DISTINCT Country FROM tblWorldData ORDER BY Country ASC', connection)

    for country in countries['Country']:
        print('Processing: %s' %country)

        country_sql = country.replace('\'','\'\'')
        query = 'SELECT DaysSinceStart, New FROM tblWorldData WHERE Country = \'%s\' ORDER BY Date ASC' %country_sql

        country_filename = country.replace(' ','_')

        createFile(query=query, file_name=country_filename, cnxn=connection)

    connection.close()