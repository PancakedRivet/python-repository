SELECT TOP 1000 IssueId, UserId, HoursSpent, Description, CreatedAt,
	CASE
		WHEN UserId = 16803 THEN 1
		WHEN UserId = 12625 THEN 2
		WHEN UserId = 11131 THEN 3
		WHEN UserId = 6427 THEN 4
	END AS UserIdMap
FROM IssueUpdate WITH (NOLOCK)
WHERE HoursSpent IS NOT NULL
	AND Visibility = 0
	--AND UserId IN (
	--	SELECT Useridentity
	--	FROM tblUser
	--	WHERE TeamId = 127
	--)
	AND UserId IN (16803)--, 12625, 11131, 6427)
	AND Description <> ''
ORDER BY CreatedAt DESC